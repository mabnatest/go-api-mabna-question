package database

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func NewPostgresDB(selectedDB string) (db *gorm.DB, err error) {
	if selectedDB == "" {
		selectedDB = viper.GetString("database.use")
	}

	selectedDB = fmt.Sprintf("database.%s.", selectedDB)

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Tehran",
		viper.GetString(selectedDB+"host"),
		viper.GetString(selectedDB+"username"),
		viper.GetString(selectedDB+"password"),
		viper.GetString(selectedDB+"db_name"),
		viper.GetString(selectedDB+"port"),
	)

	debug := viper.GetBool(selectedDB + "debug")
	gormCfg := &gorm.Config{}

	if debug {
		newLogger := logger.New(
			log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
			logger.Config{
				SlowThreshold: time.Second, // Slow SQL threshold
				LogLevel:      logger.Info, // Log level
				Colorful:      true,        // Disable color
			},
		)

		gormCfg = &gorm.Config{
			Logger: newLogger,
		}

		logrus.Infoln("database connection request:", dsn)
	}

	return gorm.Open(postgres.Open(dsn), gormCfg)
}
