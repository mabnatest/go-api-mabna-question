package main

import (
	"flag"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"go-api-mabna-question/service"
)

func init() {
	var (
		err        error
		configPath = flag.String("c", os.Args[0], "config path with json extension")
	)

	// init logger
	_ = logrus.New()

	// read config file
	flag.Parse()
	viper.SetConfigType("json")
	viper.SetConfigFile(*configPath)
	if err = viper.ReadInConfig(); err != nil {
		logrus.Panicln(err)
	}
}

func main() {
	// run web service
	if err := service.Run(); err != nil {
		logrus.Fatalln(err)
	}
}
