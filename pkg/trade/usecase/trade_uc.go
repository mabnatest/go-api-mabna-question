package usecase

import (
	"github.com/labstack/echo/v4"

	"go-api-mabna-question/models"
	"go-api-mabna-question/pkg/trade/endpoint"
	"go-api-mabna-question/pkg/trade/repository"
	"go-api-mabna-question/utils/pagination"
)

type tradeUC struct {
	repo repository.TradeRepo
}

type TradeUC interface {
	Fetch(ctx echo.Context, request *endpoint.TradeRequest) (trades []*models.Trade, paginateInfo *pagination.PaginationInfo, err error)
}

func New(repo repository.TradeRepo) TradeUC {
	return &tradeUC{
		repo: repo,
	}
}

func (tu *tradeUC) Fetch(ctx echo.Context, request *endpoint.TradeRequest) (trades []*models.Trade, paginateInfo *pagination.PaginationInfo, err error) {
	return tu.repo.Fetch(ctx, request)
}
