package trade

import (
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"

	"go-api-mabna-question/models"
	"go-api-mabna-question/pkg/trade/delivery/http"
	"go-api-mabna-question/pkg/trade/repository"
	"go-api-mabna-question/pkg/trade/usecase"
)

func Init(db *gorm.DB, e *echo.Group) {
	if viper.GetBool("database.migrate") {
		if err := db.AutoMigrate(&models.Instrument{}); err != nil {
			logrus.Errorln(err)
		}

		if err := db.AutoMigrate(&models.Trade{}); err != nil {
			logrus.Errorln(err)
		}
	}

	repo := repository.New(db)
	uc := usecase.New(repo)
	http.Init(uc, e)
}
