package endpoint

import (
	"go-api-mabna-question/models"
	"go-api-mabna-question/utils/helper"
	"go-api-mabna-question/utils/pagination"
)

type TradeRequest struct {
	helper.Request
	pagination.RequestPagination

	models.Trade
	TradeRequestFilters
}

type TradeRequestFilters struct {
	TradeID  *uint   `json:"trade_id,omitempty"`
	TradeIDs []*uint `json:"trade_i_ds,omitempty"`
}
