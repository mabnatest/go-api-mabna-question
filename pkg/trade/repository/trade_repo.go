package repository

import (
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"go-api-mabna-question/models"
	"go-api-mabna-question/pkg/trade/endpoint"
	"go-api-mabna-question/utils/pagination"
)

type tradeRepo struct {
	db *gorm.DB
}

type TradeRepo interface {
	Fetch(ctx echo.Context, request *endpoint.TradeRequest) (trades []*models.Trade, paginateInfo *pagination.PaginationInfo, err error)
}

func New(db *gorm.DB) TradeRepo {
	return &tradeRepo{
		db: db,
	}
}

func (tr *tradeRepo) Fetch(ctx echo.Context, request *endpoint.TradeRequest) (trades []*models.Trade, paginateInfo *pagination.PaginationInfo, err error) {
	var (
		query = tr.db.Model(&models.Trade{})
		total int64
	)

	if request.TradeID != nil {
		request.TradeIDs = append(request.TradeIDs, request.TradeID)
	}

	if len(request.TradeIDs) != 0 {
		query = query.Where("id in (?)", request.TradeIDs)
	}

	if err = query.Count(&total).Error; err != nil {
		logrus.Errorln(err)
	}

	request.RequestPagination.AddToQuery(query)
	request.Request.AddToQuery(query)

	if err = query.Find(&trades).Error; err != nil {
		return
	}

	paginateInfo = pagination.CreatePaginationInfo(request.RequestPagination, total)

	return
}
