package http

import (
	"go-api-mabna-question/models"
	"go-api-mabna-question/pkg/trade/endpoint"
	"go-api-mabna-question/pkg/trade/usecase"
	"go-api-mabna-question/utils/helper"
	"go-api-mabna-question/utils/pagination"
	"net/http"

	"github.com/labstack/echo/v4"
)

type tradeHandler struct {
	uc usecase.TradeUC
}

func Init(uc usecase.TradeUC, g *echo.Group) {
	handler := &tradeHandler{
		uc: uc,
	}

	tradeApi := g.Group("/trade")
	tradeApi.GET("", handler.fetch)

}

func (th *tradeHandler) fetch(ctx echo.Context) (err error) {
	var (
		request      = new(endpoint.TradeRequest)
		trades       []*models.Trade
		paginateInfo *pagination.PaginationInfo
	)

	if err = ctx.Bind(request); err != nil {
		helper.Response(ctx, http.StatusBadRequest, nil, nil, err)
	}

	if trades, paginateInfo, err = th.uc.Fetch(ctx, request); err != nil {
		helper.Response(ctx, http.StatusInternalServerError, nil, nil, err)
	}

	helper.Response(ctx, http.StatusOK, trades, paginateInfo, nil)
	return
}
