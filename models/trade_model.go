package models

import (
	"time"

	"gorm.io/gorm"
)

type Trade struct {
	gorm.Model
	InstrumentID uint        `json:"instrument_id,omitempty" gorm:"column:instrumentid"`
	Instrument   *Instrument `json:"instrument,omitempty"`
	DateEN       time.Time   `json:"date_en,omitempty"`
	Open         float64     `json:"open,omitempty"`
	High         float64     `json:"high,omitempty"`
	Low          float64     `json:"low,omitempty"`
	Close        float64     `json:"close,omitempty"`
}

func (t *Trade) TableName() string {
	return "trade"
}
