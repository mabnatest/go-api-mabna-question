package models

import "gorm.io/gorm"

type Instrument struct {
	gorm.Model
	Name string `json:"name,omitempty"`
}

func (i *Instrument) TableName() string {
	return "instrument"
}
