module go-api-mabna-question

go 1.16

require (
	github.com/labstack/echo/v4 v4.6.3
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.10.1
	gorm.io/driver/postgres v1.3.1
	gorm.io/gorm v1.23.1
)
