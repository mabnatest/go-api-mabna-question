# Mabna Rest API Question
### RESTful API microservice written by golang

## Used Library
- gorm/v2
- echo/v4
- logrus
- viper

## Build
```
go mod tidy
go build .
```

## Run
```
go-api-mabna-question -c config.json
```

## Test
```
curl --location --request GET 'http://localhost:8008/trade?limit=2&offset=0&includes=Instrument'
```