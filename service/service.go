package service

import (
	"go-api-mabna-question/database"
	"go-api-mabna-question/pkg/trade"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

func Run() (err error) {
	var (
		debug    = viper.GetBool("service.debug")
		db       *gorm.DB
		e        *echo.Echo
		apiGroup *echo.Group
		apiHost  = viper.GetString("service.host")
		apiPath  = viper.GetString("service.path")
	)

	// init http handler
	e = echo.New()

	// connect to database
	if db, err = database.NewPostgresDB(""); err != nil {
		logrus.Fatalln(err)
	}

	if debug {
		logrus.Infoln("database connection success!")
		e.Use(middleware.Logger())
	} else {
		e.Use(middleware.Recover())
	}

	apiGroup = e.Group(apiPath)

	// init packages ...
	trade.Init(db, apiGroup)

	// listen and serve Rest API
	e.Logger.Fatal(e.Start(apiHost))
	return
}
