package pagination

import (
	"math"

	"gorm.io/gorm"
)

type RequestPagination struct {
	Limit  int `json:"limit,omitempty" query:"limit"`
	Offset int `json:"offset,omitempty" query:"offset"`
	Page   int `json:"page,omitempty"`
}

type PaginationInfo struct {
	TotalItems  int64 `json:"total_items,omitempty"`
	TotalPages  int   `json:"total_pages,omitempty"`
	CurrentPage int   `json:"current_page,omitempty"`
	Limit       int   `json:"limit,omitempty"`
	Offset      int   `json:"offset,omitempty"`
}

// add request paginate to gorm DB query
func (rp *RequestPagination) AddToQuery(query *gorm.DB) (tx *gorm.DB) {
	// TODO validate request paginate data

	return query.Limit(rp.Limit).Offset(rp.Offset)
}

func CreatePaginationInfo(request RequestPagination, total int64) (paginationInfo *PaginationInfo) {
	return &PaginationInfo{
		TotalItems:  total,
		TotalPages:  int(math.Ceil(float64(total) / float64(request.Limit))),
		CurrentPage: request.Offset/request.Limit + 1,
		Limit:       request.Limit,
		Offset:      request.Offset,
	}
}
