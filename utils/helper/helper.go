package helper

import (
	"go-api-mabna-question/utils/pagination"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type Request struct {
	Includes []string `json:"includes,omitempty" query:"includes"`
	Sorts    []string `json:"sorts,omitempty" query:"sorts"`
}

type response struct {
	Status       int                        `json:"status,omitempty"`
	Data         interface{}                `json:"data,omitempty"`
	PaginateInfo *pagination.PaginationInfo `json:"paginate_info,omitempty"`
	Error        error                      `json:"error,omitempty"`
}

func Response(ctx echo.Context, status int, data interface{}, paginateInfo *pagination.PaginationInfo, err error) {
	// TODO impl error handler middleware...

	response := &response{
		Status:       status,
		Data:         data,
		PaginateInfo: paginateInfo,
		Error:        err,
	}

	ctx.JSON(status, response)
}

// add request includes data to gorm DB query
func (req *Request) AddToQuery(query *gorm.DB) (tx *gorm.DB) {
	for _, r := range req.Includes {
		query = query.Preload(r)
	}

	return query
}
